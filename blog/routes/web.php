<?php
use App\Models\Character;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('home');
});
**/
Route::get('/', function () {
	$characters=Character::all();
    return view('page.home',['characters'=>$characters]);
})->name('home');

Route::get('/about-starks', function () {
    $characters=Character::all();
    return view('page.about-starks',['characters'=>$characters]);
})->name('about-starks');

Route::get('/characters', function () {
    $characters=Character::all();
    return view('page.characters',['characters'=>$characters]);
})->name('characters');

Route::get('/forgot_password', function () {
    return view('page.forgot-password');
});

Route::get('logout', 'Auth\LoginController@logout');
Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    //only authorized users can access these routes
    Route::get('/admin', 'CharacterController@index')->name('admin');
    Route::get('/admin/manage-characters', 'CharacterController@manage')->name('characters');
    Route::get('/admin/manage-characters/create', 'CharacterController@create')->name('character.create');

    Route::post('/admin/manage-characters/store', 'CharacterController@store')->name('character.store');

    Route::get('/admin/manage-characters/{character}/edit', 'CharacterController@edit');

    Route::get('/admin/manage-characters/delete/{character}', 'CharacterController@delete');

});





