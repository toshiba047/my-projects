<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;


class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $admin = User::where('email', 'admin@admin.com')->first();
        if (!$admin) {
            User::create([
                'first_name' => 'Админ',
                'last_name' => 'Админов',
                'email' => 'admin@admin.com',
                'password' => Hash::make('password'),
            ]);
            echo "Admin created successfully. \n";
        } else {
            echo "Admin already exists. \n";
        }
    }
}