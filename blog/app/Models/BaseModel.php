<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BaseModel
 *
 * @mixin \Eloquent
 */
class BaseModel extends Model
{
    /**
     * The name of the "id" column.
     *
     * @var string
     */
    const ID = 'id';

    /**
     * The name of the "name" column.
     *
     * @var string
     */
    const NAME = 'name';

    /**
     * The name of the "label" column.
     *
     * @var string
     */
    const LABEL = 'label';

    /**
     * The name of the "title" column.
     *
     * @var string
     */
    const TITLE = 'title';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_BY = 'created_by';

    /**
     * The name of the "updated by" column.
     *
     * @var string
     */
    const UPDATED_BY = 'updated_by';

    /**
     * The name of the "deleted at" column.
     *
     * @var string
     */
    const DELETED_AT = 'deleted_at';

    /**
     * The name of the "deleted by" column.
     *
     * @var string
     */
    const DELETED_BY = 'deleted_by';
}
