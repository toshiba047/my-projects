<?php
/**
 * Created by PhpStorm.
 * User: ivan
 * Date: 04.05.19
 * Time: 16:56
 */

namespace App\Models;


class Character extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'picture_name'
    ];
}