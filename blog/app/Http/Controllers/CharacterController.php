<?php
/**
 * Created by PhpStorm.
 * User: ivan
 * Date: 04.05.19
 * Time: 17:26
 */

namespace App\Http\Controllers;


use App\Models\Character;
use Illuminate\Http\Request;

class CharacterController extends Controller
{
    public function index()
    {
        return view('admin.index');
    }

    public function manage()
    {
        $characters = Character::all();
        return view('admin.characters.manage', ['characters' => $characters] );
    }

    public function create()
    {
        return view('admin.characters.create');
    }

    public function store(Request $request)
    {
        $character = Character::create([
            'name' => $request->toArray()['name'],
            'description' => $request->toArray()['description'],
        ]);

        $request->file('thumbnail')->store('public/characters/' . $character->id);

        // ensure every image has a different name
        $file_name = $request->file('thumbnail')->hashName();

        // save new image $file_name to database
        $character->update(['picture_name' => $file_name]);

        return redirect()->to('/admin/manage-characters')->with('successMessage', 'Character added successfully');
    }
    public function delete(Character $character)
    {
        Character::destroy($character->id);

        return redirect()->to('/admin/manage-characters')->with('successMessage', 'Character deleted successfully');
    }
}