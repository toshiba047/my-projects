<div class="col-lg-3 kings">
    <a href=""><img src="/img/edard.jpg">
        <p><span>Edard Stark</span><br>Warden Of The North:</p>
        <p>"When the snows fall and the white winds blow, the lone wolf dies but the pack survives."</p>
    </a>
</div>
<div class="col-lg-3 kings">
    <a href=""><img src="/img/rob.jpg">
        <p>Rob Stark<br>King Of The North:</p>
        <p>"One victory does not make us conquerors!"</p>
    </a>
</div>
<div class="col-lg-3 kings">
    <a href=""><img src="/img/snow.jpg">
        <p>Jon Snow<br>King Of The North:</p>
        <p>"I'm not asking you to forget your dead. I’ll never forget mine."</p>
    </a>    
</div>  