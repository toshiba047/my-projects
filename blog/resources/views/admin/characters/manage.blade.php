@extends('page.template')

@section('body')
    <h2>manage characters (list all)</h2>
    <ul>
        @foreach($characters as $character)
            <li>
                <p>
                    <label>Name:  {{ $character->name }} </label> </br>
                    <label>Description: {{ $character->description }} </label> </br>
                    <label><img src="{{ asset('storage/characters/' . $character->id . '/' . $character->picture_name)}}"
                                height="50" width="50"/></label>
                </p>
                <a href="/admin/manage-characters/delete/{{ $character->id }}">Delete Character</a>
            </li>
        @endforeach
    </ul>

    <a href="{{route('character.create')}}">Add new character</a>
    <br>
    <a href="{{route('home')}}">Go Home (you are drunk)</a>
@endsection