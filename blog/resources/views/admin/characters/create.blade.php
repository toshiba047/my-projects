@extends('page.template')

@section('body')
    <div class="container">
        <div class="jumbotron padding-20">
            <h4 class="form-header">Add Character
                <i class="fas fa-shield-alt sidebar-icon-secondary" aria-hidden="true"></i>
            </h4>
            <hr class="form-underline">
            <form id="charactersForm"
                  action="{{route('character.store')}}"
                  method="POST"
                  data-toggle="validator"
                  enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="row">
                    <div class="col-xs-12 col-md-2 col-md-offset-3 form-group">
                        <label for="name">Name</label>
                        <span class="required-asterisk">*</span>
                    </div>
                    <div class="col-xs-12 col-md-3 form-group">
                        <input id="name"
                               name="name"
                               class="form-control"
                               value="{{ old('name') }}"
                               data-error="Required field!"
                               required/>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-md-2 col-md-offset-3 form-group">
                        <label for="description">Description</label>
                        <span class="required-asterisk">*</span>
                    </div>
                    <div class="col-xs-12 col-md-3 form-group">
                        <textarea class="form-control"
                                  rows="5"
                                  id="description"
                                  name="description"
                                  data-error="Required field!"
                                  required
                        ></textarea>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-6 col-md-2 col-md-offset-3 form-group">
                        <label class="btn btn-primary btn-file">
                            <i class="fa fa-upload" aria-hidden="true"></i>
                            Upload Picture
                            <input id="characterImage"
                                   type="file"
                                   name="thumbnail"
                                   style="display: none"/>
                        </label>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="col-xs-6 form-group text-left">
                        <img id="imagePreview"
                             src=""
                             style="max-width: 70%"/>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-10 text-center">
                        <button class="btn btn-primary" type="submit">
                            Create
                        </button>
                    </div>
                </div>

            </form>
        </div>
    </div>
    <script>

    </script>
@endsection