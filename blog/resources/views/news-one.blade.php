<div class="col-lg-12 news_one no_padding">
   <!-- <img src="/img/night-king4.jpg">-->
    <p>Night King A Stark?</p>
    <p>Yes, Westeros is full of terrifying and cruel characters, but obviously, no one holds a candle to the Night King. As previously mentioned, the Children of the Forest accidentally unleashed the White Walkers, but the original one, the Night King, was made from a man, and that man may have been a Stark. In the book A Storm of Swords, Old Nan says that the Night King was, according to legend, the brother of the original Bran Stark. To add a touch more irony, he was also the 13th Commander of the Night’s Watch.</p>
    <button>Read More</button>
</div>