<div class="col-lg-12 news no_padding">
    <!--<img src="/img/wolf.jpg">-->
    <p>King Of The North</p>
    <p>There have been many Kings in the North, but they have always been Starks. Bran the Builder was the first one to hold that title, with Torrhen Stark being the last when he bent the knee to Aegon. The title was restored and given to Robb during the War of the Five Kings. Now Jon Snow holds the title, and it’s only a matter of time if we find out if he’ll actually be a true King.</p>
    <button>Read More</button>
</div>