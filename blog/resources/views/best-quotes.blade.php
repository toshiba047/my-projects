<div class="col-lg-12 no_padding best_quotes">
    <h2><span>Best</span> <span>Quotes</span></h2>
    <div class="design-slider owl-carousel owl-theme">
        <div>
            <img src="/img/ned.png" align="left">
            <p>Eddard Stark</p>
            <p>"The winters are hard but the Starks will endure. We always have."
            </p>
        </div>
        <div>
            <img src="/img/rob2.jpg" align="left">
            <p>Robb Stark</p>
            <p>"Tell Lord Tywin winter is coming for him. Twenty thousand northerners marching south to find out if he really does sh*t gold."
            </p>
        </div>
        <div>
            <img src="/img/snow1.jpg" align="left">
            <p>Jon Snow</p>
            <p>“Thousands of men don’t need to die ... only one of us.” from Battle of the Bastards﻿
            </p>
        </div>
        <div>
            <img src="/img/arya.jpg" align="left">
            <p>Arya Stark</p>
            <p>“Joffrey...Cersei...Walder Frey...Meryn Trant...Tywin Lannister...The Red Woman...Beric Dondarrion...Thoros of Myr...Illyn Payne...The Mountain...The Hound"
            </p>
        </div>
        <div>
            <img src="/img/bran.jpg" align="left">
            <p>Brandon Stark</p>
            <p>“How can mirrors be real if our eyes aren't real"
            </p>
        </div>
         <div>
            <img src="/img/sansa.jpg" align="left">
            <p>Sansa Stark</p>
            <p>“I’m a slow learner, it’s true. But I learn. Thank you for all your many lessons, Lord Baelish. I will never forget them.”
            </p>
        </div>
        <div>
            <img src="/img/cat.jpg" align="left">
            <p>Catelyn Stark</p>
            <p>“I was born a Tully and wed to a Stark, I don't frighten easily.”
            </p>
        </div>
        <div>
            <img src="/img/rickon.jpg" align="left">
            <p>Rickon Stark</p>
            <p>“I'm coming with you! I'm your brother. I have to protect you!”
            </p>
        </div>
    </div>
</div>