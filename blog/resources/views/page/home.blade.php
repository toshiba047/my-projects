@extends('page.template')

@section('title', 'Home')

@section('header')
<div class="top-layout">
    <video autoplay loop muted >
        <source src="/videos/stark.mp4" type="video/mp4">
    </video>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12" >
                @include('page/sections/main-nav')
            </div>
        </div>
        <div class="row ">
            <div class="col-lg-12 second-bar">
                @include('page/sections/second-nav')
            </div>
        </div>
        <div class="row">
                @include('page/sections/motto')
        </div>
        <div class="row">
            <div class="col-lg-9 king">
                @include('page/sections/kings')
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
               @include('page/sections/latest-news')
            </div>
        </div>
    </div>
</div>
@section('body')
<section class="page-content">
    <div class="container-fluid"> 
        <div class="row">
            @include('page/sections/news')
        </div>
        <div class="row">
            @include('page/sections/news-one')
        </div>
        <div class="row">
            @include('page/sections/news-two')
        </div>
        <div class="row background_grey">
            <div class="col-lg-6 no_padding">                 
                @include('page/sections/stark-lanister')
            </div>
            <div class="col-lg-6 no_padding ">
                @include('page/sections/stark-bolton')
            </div>
        </div>
        <div class="row background_grey" >
            <div class="col-lg-6 no_padding ">                      
                @include('page/sections/stark-fray')
            </div>
            <div class="col-lg-6 no_padding ">
                @include('page/sections/stark-baelish')
            </div>
        </div>
        <div class="row">
            @include('page/sections/best-quotes')
        </div>
        <div class="row">
            @include('page/sections/contact-section')
        </div>
    </div>
</section>
@section('footer')
<div class="container-fluid">
    <div class="row">
        @include('page/sections/footer')
    </div>
</div>
@endsection