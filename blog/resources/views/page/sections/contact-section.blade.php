<div class="col-lg-12  no_padding contact-section" id="contact">
    <div class="contact-grids">
        <div class="col-md-6 contact-leftgrid">
            <h3><img src="img/raven.png"> Send me a <span>Raven</span></h3>
            <p>Send me a raven, please.</p>
            <form>
                <input type="text" class="text" value="Name " onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Name ';}">
                <input type="text" class="text" value="Email " onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email ';}">
                <textarea value="Message" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Message';}">Message </textarea>
                <input type="submit" value="SEND">
            </form>
        </div>
        <div class="col-md-6 contact-rightgrid">
            <div class="google-map">
                <img src="img/map.jpg"></img>
            </div>
        </div>
    </div>
</div>