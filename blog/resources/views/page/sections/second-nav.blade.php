<div class="thrones-logo col-lg-3">
    <img src="/img/game-logo.png">
</div>
<div class="second-navigation col-lg-9">
    <ul class="second-menu clear">
        <li class="dropdown">
            <a href="/about-starks" id="about"><button id="about_btn" class="aboutDropbtn">About Starks</button></a>
            <div id="aboutDropdown" class="about_content">
            <a href="#home">Introduction</a>
            <a href="#about">Culture</a>
            <a href="#contact">History</a>
            <a href="#contact">Season 1</a>
            <a href="#contact">Season 2</a>
            <a href="#contact">Season 3</a>
            <a href="#contact">Season 4</a>
            <a href="#contact">Season 5</a>
            <a href="#contact">Season 6</a>
            <a href="#contact">Season 7</a>
            </div>
        </li>
        <li class="dropdown">
            <a href="/characters"><button id="characters_dropbtn" class="dropbtn">Charаcters</button></a>
            <div id="charactersDropdown" class="dropdown-content">
            <a href="#home">Edard Stark</a>
            <a href="#about">Catelyn Stark</a>
            <a href="#contact">Robb Stark</a>
            <a href="#contact">Sansa Stark</a>
            <a href="#contact">Brandon Stark</a>
            <a href="#contact">Arya Stark</a>
            <a href="#contact">Rickon Stark</a>
            <a href="#contact">Jon Snow</a>
            <a href="#contact">More Charecters</a>
            @foreach($characters as $character)
                    <a href="#home">{{ $character->name }}</a>
            @endforeach
            </div>
        </li>
        <li><a href="/">News</a></li>
        <li><a href="/">Contact Us</a></li>
        <button id="mute-video">(Play/Stop Sound)
        </button>
    </ul>
</div>

<!-- @if(request()->path() != '/')
<a href="{{route('home')}}" class="home" id="home">Home</a>
@endif -->
