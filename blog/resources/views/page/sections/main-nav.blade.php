<div class="main-container">
    <img src="/img/logo3.png">
    <h1>House Stark FanSite</h1>
    <div class="top-right links">
        @if(request()->path() != '/')
            <a href="{{route('home')}}" class="home" id="home">Home</a>
        @endif

        @if(auth()->check())
            <!--<h1>Hello {{ auth()->user()->first_name }}</h1>-->
            <a href="{{route('logout')}}">Logout</a>
            <a href="{{route('admin')}}">Admin Panel</a>
        @else
            <a href="{{route('login')}}">Login</a>
            <a href="{{route('register')}}" id="register">Register</a>
        @endif
    </div>
</div>