@extends('page.template')

@section('title', 'Register')

@section('header')
<div class="top-layout">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12" >
                @include('page/sections/main-nav')
            </div>
        </div>
    </div>
</div>
@section('body')
<div class="register">
    <div class="fadeInDown">
      <div id="formContent">
        <!-- Tabs Titles -->

        <!-- Icon -->
        <div class="fadeIn first" >
          <img src="/img/logo3.jpg" id="icon" alt="User Icon" />
        </div>

        <!-- Login Form -->
        <formmethod="POST" action="{{ route('register') }}">
          <input type="text" id="login" class="fadeIn second" name="email" required placeholder="Enter Your Email">
          <input type="password" id="password" class="fadeIn third" name="login" placeholder="Enter Your Password" required>
          <input type="password" id="password" class="fadeIn third" name="login" placeholder="Repeat Your Password" required>
          <input type="submit" class="fadeIn fourth" value="Create account">
        </form>
      </div>
    </div>
</div>

@section('footer')
<div class="col-lg-12 register-footer">
    <p>Designed by <span>Vladimir Diamandiev</span></p>
</div>
@endsection

