@extends('page.template')

@section('title', 'Login')

@section('header')
<div class="top-layout">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12" >
                @include('page/sections/main-nav')
            </div>
        </div>
    </div>
</div>
@section('body')
<div class="wrapper">
  <div class="fadeInDown">
    <div id="formContent">
      <!-- Tabs Titles -->

      <!-- Icon -->
      <div class="fadeIn first" >
        <img src="/img/logo2.png" id="icon" alt="User Icon" />
      </div>

      <!-- Login Form -->
      <form method="POST" action="{{ route('login') }}">
        <input type="text" id="login" class="fadeIn second" name="login" placeholder="login" required >
        <input type="password" id="password" class="fadeIn third" name="password" placeholder="password" required>
        <input type="submit" class="fadeIn fourth" value="Log In">
      </form>

      <!-- Remind Passowrd -->
      <div id="formFooter">
        <a class="underlineHover" href="/forgot_password">Forgot Password?</a>
      </div>
    </div>
  </div>
</div>
@section('footer')
<div class="col-lg-12 login-footer">
    <p>Designed by <span>Vladimir Diamandiev</span></p>
</div>
@endsection
