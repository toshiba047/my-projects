@extends('page.template')

@section('title', 'About Starks')

@section('header')
    <div class="top-layout">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    @include('page/sections/main-nav')
                </div>
            </div>
            <div class="row ">
                <div class="col-lg-12 second-bar">
                    @include('page/sections/second-nav')
                </div>
            </div>
        </div>
    </div>
@section('body')
    <section class="about-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="introduction">
                        <h1>Introduction</h1>
                        <p>House Stark of Winterfell is a Great House of Westeros, ruling over the vast region known as
                            the North from their seat in Winterfell. It is one of the oldest lines of Westerosi nobility
                            by far, claiming a line of descent stretching back over eight thousand years. Before the
                            Targaryen conquest, as well as during the War of the Five Kings and Daenerys Targaryen's
                            invasion of Westeros, the leaders of House Stark ruled over the region as the Kings in the
                            North.
                            Their rule in the North seemingly ended after the events of the Red Wedding when House Frey
                            and House Bolton betrayed House Stark after forming a secret alliance with House Lannister,
                            during which Roose Bolton murdered King Robb Stark. Both the North and Winterfell were taken
                            over by House Bolton. However, the Bolton's hold was jeopardized when Sansa Stark escaped
                            their clutches after learning her brothers Bran and Rickon Stark were still alive and
                            reunited with her half-brother Jon Snow at Castle Black. Sansa and Jon marched on the
                            Boltons to save their younger brother Rickon, who was later murdered by Ramsay Bolton, and
                            retake their family home Winterfell. House Stark was restored to their former stature after
                            the Battle of the Bastards. The Stark victory led to House Stark's return to royal status in
                            the North with their bannermen declaring Ned Stark's illegitimate son Jon Snow as the King
                            in the North. He later abdicated his title as king in order to gain the full support of
                            Daenerys Targaryen in the Great War, becoming the Warden of the North.

                            House Stark's sigil is a grey direwolf on a white background, over a green escutcheon. They
                            are one of the few noble Houses whose family motto is not a boast or threat. Instead, the
                            House Stark family motto is a warning, one that, no matter the circumstances, will always be
                            relevant: "Winter Is Coming."</p>

                        <h1>Culture</h1>
                        <h2>Traits</h2>
                        <p>The Starks have a reputation for long faces,brown hair,and grey eyes.Some are known for
                            melancholy and iciness,while others have a wildness sometimes called "wolf blood".

                            In the current generation of Starks, several members (e.g., Arya and Bran Stark and Jon
                            Snow) have the ability to enter the minds of their direwolf pets as wargs, giving them the
                            ability to experience the senses of their direwolves and to see through their eyes. The
                            latter occurs most frequently when the children sleep,although they are able to do it at
                            will when awake, once they are more practiced.</p>
                        <h2>Customs</h2>
                        <p>House Stark traditionally buries deceased members of their family in the crypts below
                            Winterfell.The Kings of Winter and Lords of Winterfell are given a statue of their likeness,
                            sitting by their tomb, whereas other family members normally do not get one.Exceptions
                            include Brandon and Lyanna Stark, who were given statues by their brother, Lord Eddard
                            Stark,and Artos Stark. The statues of the kings and lords have stone wolves by their feet
                            and have swords placed upon their lap,which are said to keep the spirits of the dead at
                            rest, locked within their tombs.

                            House Stark traditionally follows the old gods.Following the marriage of Eddard to Lady
                            Catelyn Tully, a follower of the Faith of the Seven, a small sept was constructed at
                            Winterfell.

                            The Starks have traditionally been friends of the Night's Watch. The four youngest men to
                            have served as Lord Commander, including Osric Stark, were brothers, sons, or bastards of
                            Kings in the North.</p>
                        <h1>History</h1>
                        <p>Before the Targaryen conquest, the leaders of House Stark ruled over the region as the Kings
                            in the North.The house traces its roots to Bran the Builder, a legendary First Man who lived
                            during the Age of Heroes and founded House Stark. The Starks ruled as Kings of Winter over
                            one of the many smaller, petty First Men kingdoms that were established in the North, with
                            their chief rivals for domination being the Red Kings of House Bolton. Eventually the Starks
                            united the North under them. They defeated the Boltons, drove pirates away from the White
                            Knife, slew the last Marsh King and wed his daughter for the Neck, and, according to legend,
                            King Rodrik Stark wrestled with an ironborn for Bear Island and won. Several centuries
                            before the Targaryen conquest, Karlon Stark, a younger son of the King in the North, was
                            awarded lands in the eastern regions of the North after successfully putting down a
                            rebellion led by House Bolton. Over time Karlon's seat of Karl's Hold came to be known as
                            Karhold, and the Starks that descended from him became known as Karstarks.

                            King Torrhen Stark was on the throne at the time of the Targaryen conquest and marched his
                            army south to face them. He surrendered when he saw the Targaryens' greater host and their
                            dragons, believing that fighting was futile. He was made Lord Paramount of the North and
                            served the Targaryens as Warden of the North, thus escaping the fates of House Gardener of
                            the Reach and House Hoare of the Riverlands whose lords refused to bend the knee and were
                            rendered extinct after failing to resist the invaders. Thereafter, Torrhen was known as "the
                            king who knelt", though those who criticize him for this often forget that they are only
                            alive to do so thanks to his surrender.</p>
                        <h2>Season 1</h2>
                        <p>Lord Eddard "Ned" Stark became Hand of the King to King Robert Baratheon after the death of
                            Jon Arryn. Ned took his daughters Sansa and Arya Stark to King's Landing. It was there that
                            he discovered the true lineage of Robert's assumed children. Upon Robert's death, Ned
                            publicly declared that Joffrey Baratheon was the product of incest between Queen Cersei
                            Lannister and her twin brother, Jaime Lannister, and therefore was not the rightful heir to
                            the Iron Throne. He was subsequently executed for treason. His firstborn son and heir Robb
                            Stark was declared the King in the North by his bannermen (the first in 300 years) and
                            fought to secede from the Seven Kingdoms in what would become the War of the Five
                            Kings.Sansa Stark becomes a political captive of House Lannister, while Arya escapes King's
                            Landing with Yoren of the Night's Watch.</p>
                        <h2>Season 2</h2>
                        <p>Robb Stark continues his campaign against the Lannisters, winning a trio of victories in the
                            Riverlands and a further three victories in the Westerlands at Oxcross, The Yellow Fork and
                            The Crag. Bran Stark and Rickon Stark leave Winterfell after a traitorous Theon Greyjoy
                            takes control of Winterfell. Theon pursues them, but is unable to find them, and instead
                            burns two farmer's sons and declares that the bodies are those of Bran and Rickon Stark,
                            leading the majority of Westeros believing that all the male Stark heirs are dead (as Jon
                            Snow is a bastard son of Ned Stark and sworn to the Night's Watch)</p>
                        <h2>Season 3</h2>
                        <p>Although Robb continues to win every battle that he fights, House Baratheon of King's Landing
                            secures an alliance with House Tyrell that effectively leads to victory over Stannis
                            Baratheon at the Battle of the Blackwater. As Robb continues to march further in the
                            Westerlands, many of his bannermen, such as Rickard Karstark who lost two of his sons, begin
                            to lose faith in King Robb and are aware that he broke his vow to marry one of the daughters
                            of Lord Walder Frey when he married Talisa Maegyr. Robb and his host arrive at Harrenhal to
                            discover that Ser Gregor Clegane has put 200 northern prisoners to the sword and left
                            Harrenhal without a fight. King Robb received news at Harrenhal that his grandfather Hoster
                            Tully has died, and Robb goes to Riverrun to attend the funeral, leaving Roose Bolton and
                            his men in charge of Harrenhal.At Riverrun, Robb chides his uncle, the new Lord Edmure
                            Tully, for attacking Ser Gregor Clegane at the Stone Mill, forcing Gregor's host to retreat
                            to Casterly Rock and losing more than 200 men in the battle. Later Lord Rickard Karstark
                            murders the two Lannister captives, Willem and Martyn Lannister, which causes Robb to
                            execute Lord Rickard, making the Karstarks abandon King Robb. With his host diminished due
                            to the Karstarks' abandonment, Robb opts to have the support of House Frey but finds it
                            difficult because of his broken vow. However, the Freys agree to support Robb if his uncle
                            Edmure marries Roslin Frey. Robb and Edmure agree to the terms of the Freys and prepare to
                            head to the Twins for the wedding.
                            Robb and all of his bannermen arrive at the Twins where Lord Walder Frey begins preparations
                            for the wedding. All the northern lords of Robb's host attend the wedding while the northern
                            army camps outside during the feast. After Edmure and Roslin are taken away for the bedding,
                            the Freys and Boltons betray King Robb and massacre them in an event known as the Red
                            Wedding, secretly orchestrated by Tywin Lannister. King Robb, Queen Talisa, Robb's mother
                            Catelyn, Robb's direwolf Grey Wind, the present Northern lords and most of the Northern
                            army, are murdered during this event.In the aftermath of the Red Wedding, the Freys sew Grey
                            Wind's head to King Robb's body as a final insult to the King in the North. House Stark is
                            stripped of its lordship in the North and is given to House Bolton, making Roose Bolton
                            Warden of the North for his contribution to Robb's death.
                            Bran and Rickon, who have been travelling north along with Osha, Hodor, Jojen Reed and Meera
                            Reed, as well as their two direwolves, Summer and Shaggydog, part ways, with Bran, Jojen,
                            Hodor, Meera and Summer headed beyond the wall and Rickon, Shaggydog and Osha headed to the
                            Last Hearth to seek shelter from House Umber.</p>
                        <h2>Season 4</h2>
                        <p>Bolton has roughly half of his whole army trapped south of the Neck, since the ironborn
                            captured Moat Cailin, and cannot properly rule the North.His son Ramsay takes back the Moat
                            and the Boltons ride to Winterfell in order to rule the North from there, as it is the
                            ancient stronghold of House Stark and the capital of the North.[7]
                            Tywin Lannister's eventual plan was to return the North to the Starks under Lannister
                            control via the possible son of Tyrion Lannister and Sansa Stark, but Tyrion and Sansa each
                            escape King's Landing and separate as the result of King Joffrey's assassination. Tywin
                            himself was killed before his plan could come to fruition.
                            Bran makes his way further north, eventually reaching the weirwood tree occupied by The
                            Children of the Forest and the Three-Eyed Raven.</p>
                        <h2>Season 5</h2>
                        <p>Petyr "Littlefinger" Baelish secretly returns Sansa to Winterfell in the hopes of wedding her
                            to Ramsay Bolton. The Boltons are eager to cement their control over the North, particularly
                            since they are now living in a Westeros without solid Lannister support as Tywin Lannister
                            is now deceased. Littlefinger tells Sansa that she is perfectly situated; Stannis Baratheon
                            is likely to liberate Winterfell soon, and will likely declare her Wardeness of the North.
                            Even if he doesn't, Sansa will be perfectly placed to retake Winterfell from within House
                            Bolton. Although Sansa doesn't quite realize it, there are many in the North who are already
                            taking heart from the fact that there is again a Stark in Winterfell.
                            Although Sansa's attempt to contact her supporters (and unknown to her, Brienne of Tarth)
                            ends in failure and the brutal death of a Winterfell servant, Sansa herself begins to have
                            hope for her House after she forces a distressed Reek to admit that he killed two farm boys
                            and not Bran and Rickon. She later flees Winterfell with Theon Greyjoy during the Battle of
                            Winterfell.
                            Jon Snow, Lord Commander of the Night's Watch at the time, is betrayed by his men and
                            stabbed to death by Alliser Thorne, Olly, and several other Black Brothers.</p>
                        <h2>Season 6</h2>
                        <p>Sansa flees from Winterfell with Theon at her side; they ford a wide river at great risk and
                            take shelter in an attempt to conceal themselves from Ramsay Bolton's hounds. They are
                            cornered and nearly caught despite Theon's attempt to ward them off by revealing himself.
                            However, they are saved by Brienne and a now-trained Pod, who eliminate Ramsay's hunters.
                            With the threat neutralized, Brienne again offers herself to serve Sansa in order to keep
                            the oath she made to her mother.Unlike before, Sansa accepts, and the two recite the oath of
                            service, with assistance from Pod. Sansa is heading to her resurrected half-brother Jon at
                            Castle Black as he will protect her and keep her safe. However, Theon opts to leave Sansa's
                            side - with her blessing - to return home to Pyke, knowing that he could never make amends
                            for his grave misdeeds against his former adoptive family.
                            Smalljon Umber trades Rickon Stark and Osha over to Ramsay Bolton, the new Warden of the
                            North, in order to secure an alliance with House Bolton against the Free Folk.
                            Meanwhile, Bran Stark experiences a vision of his family while training with the Three-Eyed
                            Raven. He witnesses several of his family members including his father Ned, uncle Benjen,
                            and his aunt Lyanna Stark.
                            At Castle Black, the Red Priestess Melisandre resurrects Jon Snow from the dead.Jon decides
                            to leave the Night's Watch on the grounds that his watch has ended through his death. Sansa,
                            accompanied by Brienne and Podrick, reunites with her brother Jon. Sansa urges Jon to help
                            her re-take Winterfell from the Boltons. Later, Jon finally agrees after receiving a letter
                            from Ramsay stating he has their younger brother Rickon and demanding the return of Sansa.
                            Jon resolves to march on the Boltons, leading an army of able-bodied Free Folk under Tormund
                            and gathering forces he and Sansa plan to rally among the remaining loyal houses in order to
                            save Rickon and reclaim Winterfell from the Boltons.
                            Jon and Sansa are rebuffed by House Glover and receive no response from Houses Cerwyn and
                            Manderly. They recruit a few hundred soldiers from Houses Mormont, Hornwood, and Mazin.
                            Sansa argues they need more men but Jon resolves to march on Winterfell with the forces they
                            have, arguing there is no more time left. Sansa sends a further raven to an unknown party,
                            likely Littlefinger.
                            During the Battle of the Bastards, Jon Snow attacks Winterfell with 2,405 men. Rickon Stark
                            is killed by Ramsay Bolton as the battle begins. As Jon is on the verge of losing, the
                            Knights of the Vale arrive to join the battle with Petyr Baelish. Jon and Sansa reclaim
                            Winterfell after Jon beats Ramsay in a one-to-one combat, leaving Ramsay's fate for Sansa to
                            decide. Jon orders his brother Rickon to be buried in the crypt next to his father. Sansa
                            visits the dog kennels and kills a bloodied Ramsay with his own dogs. With Ramsay's death,
                            House Bolton is rendered completely extinct, Winterfell finally returns to her rightful
                            owners and the Red Wedding is partly avenged.
                            After the battle, the Northern lords, along with the Lords of the Vale meet in the hall at
                            Winterfell to treat with House Stark and discuss provisions for the Winter. Lord Cerwyn
                            floats the idea of hiding behind castle walls, but Jon Snow rejects this, reminding the
                            Northern houses of their true enemy and that this enemy "doesn't wait out the storms, he
                            brings the storm." Lady Lyanna Mormont berates the Manderlys, the Cerwyns and the Glovers
                            for not supporting House Stark in the Battle of the Bastards and declares Jon Snow the King
                            in the North, despite his bastardy, as he has the blood of Eddard Stark, the last rightful
                            Warden of The North, and Robb Stark, the last King in the North. Lyanna Mormont is then
                            followed by Lord Wyman Manderly, Robett Glover, and the rest of the gathered lords, who all
                            declare Jon their king. Meanwhile, in the Riverlands, Arya Stark personally slays Lord
                            Walder Frey and every member of the house who participated in the Red Wedding massacre,
                            finally avenging the North.</p>
                        <h2>Season 7</h2>
                        <p>At Winterfell, the King in the North Jon Snow organizes the defense of the North against the
                            Night King and his army of the dead. He asks that all maesters start searching for
                            dragonglass, stressing it is now more valuable than gold due to its effectiveness against
                            the White Walkers. Jon also requests that Tormund and his people man Eastwatch-by-the-Sea as
                            Tormund and the other wildlings were present at Hardhome and have seen the Night King and
                            Tormund agrees to defend this castle. Jon orders for all able-bodied men and boys aged 10 to
                            60 in his kingdom to be trained in combat in order to defend against the encroaching threat
                            of the White Walkers. Because having only half of the population in the North fighting the
                            White Walkers is not enough, he also orders that every woman and girl should also be trained
                            and equipped as well. When Robett Glover questions Snow, Lady Lyanna Mormont remarks that
                            girls will not remain idle and volunteers to help, giving her assurance that every girl on
                            Bear Island will be trained alongside the boys. Sansa Stark urges her half-brother to strip
                            the Umbers and Karstarks of their castles as punishment for turning against the Starks.
                            However, Jon advocates forgiveness and insists that children will not be punished for the
                            crimes of their fathers. Despite Sansa's continued insistence, Jon insists his decision is
                            final and summons Ned Umber and Alys Karstark - both of whom are not even in their teens. He
                            asks them to reaffirm their loyalty to House Stark. They oblige and kneel before King Jon.
                            Jon says that the mistakes of the past don't matter anymore. Petyr Baelish watches the
                            proceedings with a smile
                            In private, Jon chides Sansa for questioning his decision-making in front of the other lords
                            and ladies. He tells Sansa that while she is his sister and she can question his decisions,
                            doing so when he is publicly addressing the Northern lords and ladies undermines his
                            position with them. When Sansa responds that the late Joffrey Baratheon did not tolerate
                            dissent, Jon reassures her that he is not Joffrey. Sansa tells Jon that she knows he is
                            nothing like Joffrey and assures Jon that he is good at leadership, but she wants him to be
                            wiser than their late father and brother. She confides that their father sought to protect
                            her from the harshness of reality, including swearing. Maester Wolkan then delivers a
                            message from the newly-crowned Queen Cersei Lannister; though she is apparently not opposed
                            to House Stark reclaiming the North from House Bolton, she demands that they submit to her
                            authority. While Jon is preoccupied with preparing to fight the Night King, Sansa warns him
                            not to underestimate Cersei.

                            Beyond the Wall, Bran Stark and Meera Reed reach the gate beneath Castle Black. They are
                            greeted by the Acting Lord Commander Eddison Tollett and several armed Black Brothers. Edd
                            asks if they are Wildlings, but Meera introduces herself and Bran. When Edd asks them to
                            verify their identities, Bran responds by "recognizing" Edd from the conflicts at the Fist
                            of the First Men and Hardhome, observing that he has seen the army of the dead. Edd decides
                            to bring the two of them inside, glancing nervously at the increasingly hostile lands beyond
                            the Wall.

                            At Winterfell, Jon Snow, Sansa Stark, and Davos Seaworth discuss a letter they received from
                            Tyrion Lannister about a potential alliance. Jon asks for Sansa's opinion and while Sansa
                            remembers that Tyrion was kind to her and was unlike the rest of his family, she wonders if
                            the message is truly from Tyrion. Jon confirms the letter's authenticity as it ends with the
                            line "all dwarfs are bastards in their fathers' eyes," which is what Tyrion told Jon when
                            they first met. Even so, Sansa worries it is too big a risk and Jon concedes that now is not
                            the right time to go to Dragonstone. Davos opines that Dany will make a good ally in the war
                            to come against the White Walkers.

                            Some days later, Maester Wolkan brings Jon Snow a message from Samwell Tarly in the Citadel.
                            Jon gathers the Northern lords in the main hall and announces that Sam's letter reveals
                            there is a vein of dragon glass beneath Dragonstone. He adds that Lord Tyrion has invited
                            him to Dragonstone to meet with Queen Daenerys and announces his decision to travel to
                            Dragonstone to convince Daenerys to join their fight against the White Walkers, declaring
                            that he plans to travel with Davos to White Harbor and sail to Dragonstone island. Lords
                            Yohn Royce and Robett Glover voice their opinions that a Targaryen cannot be trusted. Lady
                            Lyanna Mormont urges the King in the North to stay at home. Jon accepts that he is taking a
                            risk but stresses that the fight against the White Walkers is more important, and they need
                            Daenerys' aid if the North is to be saved. Jon emphasizes that the North is his home, it is
                            part of him, and he will never stop fighting for it. Sansa reiterates her objection to Jon's
                            leaving, and Jon tells Sansa that he is appointing her as ruler of the North in his absence
                            as she is his sister, regent, and a Stark. Sansa accepts.

                            In the Riverlands, Arya Stark encounters her old friend Hot Pie at the Inn at the
                            Crossroads, accosting him to give her some pie and ale. Hot Pie then reveals to her that he
                            Boltons are now dead, killed during the Battle of the Bastards by her half-brother Jon Snow,
                            who has been named King in the North. Arya decides to head north.
                            At Winterfell, Sansa and Petyr Baelish learn from Maester Wolkan that they have about 4,000
                            bushels of wheat. Sansa realizes that they don't have enough food for the coming winter. She
                            advocates building granaries to stockpile for a famine. Sansa also orders Yohn Royce to see
                            that the armor made for their armies is outfitted with leather to keep warm. While walking,
                            Baelish and Sansa talk about the threat of Cersei. Petyr urges her to fight every battle and
                            to look for threats in every corner. They are then interrupted by a guard who tells Lady
                            Stark that she has received a visitor, who turns out to be her younger brother Bran Stark,
                            accompanied by Meera Reed.

                            Following a tearful reunion, the two siblings retreat to the Godswood, where Sansa tells
                            Bran how she wishes Jon were there with them at Winterfell. Bran agrees, noting that he
                            needs to speak to Jon. When Sansa points out that Bran is the rightful Lord of Winterfell
                            since he is the last remaining true-born son of Ned Stark, he refuses the position, stating
                            that he is the Three-Eyed Raven and thus can't be any sort of Lord. Sansa begs for Bran to
                            explain what that means, and Bran then demonstrates his newly-acquired power to a skeptical
                            Sansa by recalling details from the night of her marriage to Ramsay Bolton. Startled, Sansa
                            walks away in shock and tears.

                            Jon and Davos arrive at Dragonstone, and are immediately greeted by Tyrion and Missandei.
                            Upon meeting, Tyrion address Jon as the bastard of Winterfell, while Jon addresses him as
                            the dwarf of Casterly Rock – the two share a friendly grin. Jon observes that Tyrion has
                            picked up some scars. Jon also introduces Davos while Tyrion introduces Missandei, who
                            requests that they surrender their weapons. Jon and his entourage hand over their weapons to
                            Daenerys's Dothraki guards.

                            On the walk to the castle, Missandei walks with Davos and tells him that she comes from the
                            island of Naath. Davos remarks that it was a paradise full of palm trees. Jon and Tyrion
                            talk about Sansa Stark's marriage to Tyrion. Tyrion assures Jon that it was a sham and was
                            never consummated, and remarks that she is a lot smarter than she lets on, to which Jon
                            agrees. While Jon is aware about the fate of the previous Starks who had met with the Mad
                            King, Jon insists that he is not a Stark. Jon and Davos are startled at the sight of Drogon
                            and Viserion flying low over the causeway and dive to the ground, while an amused Missandei
                            and Tyrion retain their composure. Offering Jon a hand up, Tyrion says he wishes he could
                            tell Jon he'll get used to the dragons – but no one is quite used to them except their
                            mother, who is waiting for Jon within.
                            At Winterfell, Littlefinger meets with the recently-returned Bran Stark. Apparently hoping
                            to exploit disunity among the Stark children, he tries to ingratiate himself with Bran, Ned
                            Stark's last trueborn son and rightful heir to Winterfell. Littlefinger gives Bran a gift,
                            the very same Valyrian steel dagger that a cutthroat tried to kill him with while he was in
                            a coma right after he was pushed from one of Winterfell's towers. Bran is still emotionally
                            detached, however, from all of his visions as the new Three-Eyed Raven, and is generally
                            uninterested, only absent-mindedly asking who the dagger belonged to. With a wry look,
                            Littlefinger responds that, in a way, this is the question which started the entire War of
                            the Five Kings. Bran's mother Catelyn Stark took the dagger south with her to King's Landing
                            to try to find who it belonged to, convinced the Lannisters sent the cutthroat to kill Bran
                            (and left it there in Ned's possession, from whom Littlefinger recovered it). Littlefinger
                            tries to manipulate Bran by remarking on how much chaos he must have lived through to get
                            back to Winterfell. In response, Bran looks at Littlefinger and says, "Chaos is a ladder,"
                            quoting back Littlefinger's own words to him, which Littlefinger said to Varys in King's
                            Landing years before and which Bran couldn't possibly have been physically present to hear
                            himself. Visibly unnerved, Littlefinger is startled by Meera Reed entering the room. He
                            takes his leave of Bran, calling him "Lord Stark", though again Bran insists he's not a
                            Lord. After Littlefinger leaves, Meera notices Bran's new wheelchair. He explains Maester
                            Wolkan built it for him. She tells him that she has come to tell him she's leaving and say
                            goodbye; she promised to keep him safe, and now that he's back in Winterfell surrounded by
                            his Stark forces, he's as safe as anyone will be before the coming war. Meera explains that
                            when the White Walkers arrive, she wants to be with her family, so she is heading back to
                            the Neck to assist the Crannogmen. She says he doesn't need her in Winterfell anymore; Bran
                            agrees with her and with blank emotional affect, simply thanking her for her help. Meera
                            grows angry that this is all he has to say after everything they went through, when Hodor
                            and Summer and even her own brother Jojen died for Bran. He then explains that he isn't
                            really "Bran Stark" anymore, but the Three-Eyed Raven. He "remembers" the events of Bran
                            Stark's life, but now "remembers" vast amounts of other accumulated memories from centuries
                            upon centuries. Everything that once affected "Bran Stark" now seems distant and trivial to
                            him. In horror, Meera cries that he "died" in the cave and leaves.

                            Arya Stark finally returns to Winterfell, after leaving years before with her father, Sansa,
                            and King Robert Baratheon's entourage, right after Bran's fall from the tower. Arya rides up
                            to the gates and dismounts, but the guards don't believe her when she says she is in fact
                            Arya Stark, convinced that Arya has been dead for years. Arya asks that they send word to
                            Maester Luwin and Rodrik Cassel, hoping either can prove her identity but unaware they are
                            both dead. The guards gruffly say that no one by those names is there, so Arya asks for Jon.
                            The two say he'd just left Winterfell. Arya asks who is in charge of Winterfell then, and
                            they say "Lady Stark" (who Arya realizes is her sister). They try to brush her aside, but
                            she dodges them with her assassin's reflexes, and insists that one way or another, she's
                            getting in. She explains to them that if she is Arya, they'll be in a lot of trouble for
                            turning her away, and if she isn't, she won't last long in Winterfell anyway. Mildly
                            concerned, they agree to at least let her in the courtyard but insist that she stay put
                            while they consult Sansa so they can disprove her identity. As soon as they take their eyes
                            off her, however, she slips away. The two guards go to inform Sansa and try to wave it aside
                            as just some impostor, but when they mention she asked for Luwin and Cassel, she instantly
                            realizes it must be Arya, and already knows where she has gone.

                            Sansa finds Arya where she expected, in the crypts looking over their father Ned's grave.
                            They are happy to see each other but so much has happened to both of them in the past few
                            years that they are at first awkward, unsure of what to say. Arya asks if she has to call
                            Sansa "Lady Stark" now, to which Sansa firmly insists yes, and laughs. They smile and hug,
                            though still a bit unsure. Arya notes that Jon left her in charge and smiles when Sansa says
                            that she hopes Jon will be back soon. He will be both surprised and happy to see Arya, Sansa
                            remembering how happy Jon was to see her when they were reunited. The sisters then look
                            sadly on their father's grave statue. Arya says it doesn't really look like him. Sansa
                            acknowledges that everyone who knew his face well is dead. Arya points out they're not. Arya
                            then asks if Sansa killed Joffrey as everyone believes. She explains she actually didn't,
                            though she wished she had. Arya remarks that he was always at the top of her "list". This
                            confuses Sansa, and Arya explains that she'd been keeping a list of everyone she was going
                            to kill, at which they both laugh. Finally Sansa asks how Arya got back, but she only says
                            her road wasn't a pleasant one. Sansa says hers wasn't either. They hug again, earnestly and
                            warmly. Sansa then informs Arya that Bran is home too. Arya is elated, but her face falls
                            when Sansa makes no mention of Rickon, immediately realizing that Rickon is dead.

                            Sansa brings Arya to Bran in the Godswood, where he is lost in thought by the Weirwood heart
                            tree. Arya is saddened to see him so paralyzed. Still somewhat detached even at the sight of
                            Arya, he says he isn't surprised she's alive because he saw her at the Crossroads. Arya is
                            confused, and Sansa explains that Bran is having "visions" now. Bran says he thought Arya
                            was going to King's Landing, and when Sansa asks why she would head there of all places, he
                            again startles them both by saying it's because Cersei is on her list of names (which he
                            can't possibly be aware of through normal means). Sansa asks who else is on her list, but
                            she says most of them besides Cersei are actually dead already. They then remark on the
                            Valyrian steel dagger in his lap, and he explains that Littlefinger gave it to him, thinking
                            he'd want it. Despite it being such a horrible keepsake that nearly killed him and
                            indirectly set off a chain of events leading to his parents' deaths, he is still listless
                            and disinterested in it. Arya is confused as to why a common cuthroat would have a rare,
                            priceless blade of Valyrian steel. Bran matter-of-factly says that someone very wealthy
                            wanted him dead, and gave it to the assassin. Sansa acknowledges she doesn't actually trust
                            Littlefinger and he'd never give anyone anything unless expecting something in return. Bran
                            says that doesn't matter, because he doesn't even want it. Instead, Bran hands it to Arya
                            and says she can have it, being "wasted on a cripple". Sansa looks down, dejected at how her
                            brother considers his own physical state to be useless. Sansa, Bran, and Arya - the three
                            remaining trueborn Stark children - proceed back to Winterfell's castle courtyard together,
                            with Arya pushing Bran in his wheelchair.
                            At the Godswood in Winterfell, Bran Stark wargs into a flock of ravens to fly over the Wall
                            into the Lands of Always Winter. Through the ravens, he sees the Army of the Dead led by the
                            White Walkers and the Night King, traveling south towards Eastwatch-by-the-Sea. His
                            reconnaissance continues until the Night King sees the ravens, at which Bran severs the
                            connection. He tells Maester Wolkan that they need to send ravens at once. Later, in
                            Winterfell's council chamber, Arya Stark observes Regent Sansa Stark presiding over a
                            meeting of the Northern lords. Saying that the King in the North should stay in the north,
                            Lord Robett Glover and Lord Yohn Royce of The Vale of Arryn imply they made a mistake in
                            their choice of ruler and that she take over power in the absence of her half-brother Jon
                            Snow. However, Sansa insists Jon Snow is their true ruler who is doing what he believes is
                            right for their people and that she is his regent. Following the meeting, Sansa confides her
                            frustration in the Northern lords with Arya, who calls Sansa out for diplomatically handling
                            their concerns instead of shutting the lords down. Arya thinks that she should not let the
                            lords get away with insulting Jon and hints at assassinating them. Sansa disagrees, at which
                            Arya calls attention to the fact that Sansa is using their parents' chambers and that she
                            still thinks she's better than anyone else – perhaps she even thinks that she will need the
                            support of the northern lords if she wishes to seize the North for herself, even if she
                            tries not to think of it. Disturbed at her younger sister's homicidal streak and hurt by the
                            accusations, Sansa tells Arya that she has "work to do."

                            Much later, Arya stalks Petyr Baelish as he is walking through the grounds of Winterfell.
                            She follows Petyr into his personal quarters and catches him chatting with Maester Wolkan.
                            She eavesdrops on Petyr asking Wolkan if he is sure that "this" is the only copy. Petyr
                            replies that Lady Sansa Stark thanks him for his services. After Petyr and Wolkan have left,
                            Arya enters Petyr's chamber and rummages through his study and furniture. While searching
                            through his mattress, she finds a scroll written by Sansa. This turns out to be the scroll
                            that Sansa wrote to their late brother Robb Stark urging him to bend the knee to King
                            Joffrey Baratheon. Arya, unaware that Sansa had written the letter under duress from Cersei
                            in an attempt to save their father Eddard Stark, looks horrified. Arya scrunches up the
                            letter and sneaks out of the room, oblivious to a grinning Littlefinger watching from behind
                            a wall.

                            At Dragonstone Jon Snow is walking on the grounds of the island when Daenerys arrives on the
                            back of her dragon Drogon. Drogon roars at Jon at first and stretches out his head to face
                            the King in the North. Drogon calms down and recognizes Jon as a friend of his master (and
                            possibly sensing his Targaryen ancestry), allowing him to stroke his snout, much to Dany's
                            surprise. When Jon Snow (under slight duress) replies that the dragons are beautiful beasts,
                            Dany responds that the dragons are her children. When Jon Snow observes that she was not
                            gone for long, Daenerys curtly replies that she has fewer enemies to deal with. Daenerys
                            then asks Jon about the Battle of the Bastards and him "taking a knife in the heart for his
                            people". Without going into detail, Jon replies that Davos Seaworth likes to embellish
                            things. Their conversation is interrupted by the return of a recently-healed Jorah Mormont.
                            Daenerys introduces Jorah to Jon, who says he worked under Jorah's father in the Night's
                            Watch.

                            Later, at the Chamber of the Painted Table, Jon tells Daenerys about the news of his
                            half-brother Bran and half-sister Arya Stark's return to Winterfell. He warns Daenerys about
                            Bran's vision of the Army of the Dead marching towards Eastwatch-by-the-Sea. Tyrion is
                            present and proposes bring a Wight south in order to prove that the Army of the Dead and the
                            White Walkers are real. Varys opines that it is suicide trying to appeal to Queen Cersei
                            Lannister but Tyrion argues that he can persuade his brother Jaime. Davos also thinks such a
                            mission is risky even for a smuggler like him. Jorah volunteers to go north to help capture
                            a Wight while Jon volunteers to lead the expedition. Apparently on the verge of tears about
                            the idea of Jon leaving, Daenerys responds that she did not give Jon permission to leave but
                            Jon reminds her that he is the King in the North. He tells her she has the power of life and
                            death over him but that he trusted her even though she was a stranger. He pleads with her to
                            return the favor by trusting him. Later, Davos and Tyrion chat about smuggling before
                            embarking on their mission to infiltrate King's Landing. Davos and Tyrion return with Gendry
                            to Dragonstone. At the dragonglass mines, they meet Jon Snow, who is supervising the
                            diggings and excavations. Gendry remarks that Jon is a lot shorter than he expected – and
                            immediately blurts out his true parentage, on the assumption that Jon will value honesty and
                            will appreciate the idea of Ned Stark and Robert Baratheon's bastards joining forces. Gendry
                            volunteers to accompany Jon on his mission to the North to capture a wight and convince
                            Queen Daenerys and Queen Cersei that the true war lies to the North. As Jon Snow and his
                            party including Jorah Mormont prepare to depart on boats for Eastwatch-by-the-Sea, Dany and
                            her entourage arrive and bid Jorah farewell. Jorah quips that he is used to saying farewell.
                            Jon warns Daenerys that there is a chance that he might not return and wishes her well in
                            the "wars to come." Dany and Tyrion watch as Jon Snow and his party depart on their boats
                            for Eastwatch.

                            At Winterfell, Arya Stark talks to her sister Sansa Stark about borrowing Bran Stark's bow
                            and arrow. She tells Sansa that she practiced several times until she finally hit the
                            bullseye. Arya recalls that their father Eddard Stark had been watching and clapped his
                            hands in praise of her accomplishments. Arya reasons that their father knew that the rules
                            were wrong but that his daughter was in the right. She then confronts Sansa about her
                            alleged role in their father's death. Arya presents the letter that Sansa had written to
                            their late brother Robb Stark urging him to come and bend the knee to King Joffrey
                            Baratheon. Sansa replies that the Queen Mother Cersei Lannister forced her to do it under
                            duress. Arya counters that she was not tortured and that she saw Sansa at Ned's execution;
                            Sansa retorts that Arya did nothing to stop their father's execution either. Arya chastises
                            Sansa for betraying their family but Sansa responds that they have only returned to
                            Winterfell because of her, while Arya traveled the world in pursuit of her own agenda. Sansa
                            adds that their half-brother Jon Snow was saved from defeat when Petyr Baelish and the
                            Knights of the Vale came to their rescue and insists Arya would not have survived the
                            torments she endured at the hands of Joffrey and Ramsay. Sansa demands to know where Arya
                            found the letter and chides her younger sister that Cersei would be pleased to see them
                            fighting but Arya is still bitter towards Sansa. She realizes that while Jon would
                            understand the difficult circumstances Sansa was under when she wrote the letter, Sansa is
                            afraid the Northern lords will discover it and turn on her, including Lyanna Mormont. Arya
                            adds that Lyanna is younger than Sansa was when she wrote this letter but argues Lyanna
                            wouldn't agree with Sansa's defense that she was a child at the time. While recognizing that
                            Sansa wrote the letter out of fear, a bitter Arya says that she prefers to embrace anger
                            over fear. Later, Sansa asks Petyr Baelish about where Arya got the letter from, unaware
                            that Baelish orchestrated the entire incident. Sansa tells Petyr that she is commanding
                            20,000 men who answer to Jon but not to her. Petyr tells Sansa that the men will trust her
                            because she can rule. Sansa does not trust the loyalty of the Northern lords, citing their
                            history of switching sides. She counters that the discovery of the letter will turn her
                            liege lords and men against her. Sansa confides in Petyr about her strained relations with
                            Arya. Petyr suggests that Sansa talk to Brienne of Tarth because she has sworn to protect
                            both of Lady Catelyn Stark's daughters from harm's way. Trusting Baelish, Sansa accepts his
                            advice. The following morning, Maester Wolkan informs Sansa that they have received a letter
                            from Queen Cersei. Sansa meets with Brienne, who advises her not to leave Winterfell.
                            Instead, Sansa decides to send Brienne as her representative since she could reason with
                            Jaime Lannister. Brienne warns that it is too dangerous for her to leave Sansa alone at
                            Winterfell with Petyr. Sansa insists that her guards and men are loyal to her but Brienne
                            warns that Petyr might be bribing them behind her back. Brienne offers to leave her squire
                            Podrick Payne, whose swordsmanship has improved, but Sansa insists that she can take care of
                            herself.

                            Beyond the wall Jon Snow and his ranging party travel through the lands beyond the wall on
                            their mission to capture a wight. While walking, Jon and Jorah Mormont also chat about their
                            relationships with their fathers Eddard Stark and Jeor Mormont respectively. They say that
                            their fathers were good and honorable men and did not deserve their deaths. Jon tells Jorah
                            about the brutal death of Jeor at the hands of the Mutineers and that Eddard was beheaded.
                            Jon tries to return Jorah his father's Valyrian sword Longclaw but Jorah tells him that he
                            is not worthy to bear his father's sword and tells Jon to keep it. While trudging through a
                            snowstorm, Jon and his party sight a massive snow bear with blue eyes approaching them. The
                            snow bear turns out to have been resurrected by a White Walker. The monstrous creature mauls
                            and kills three of their company. Beric and Thoros manage to set the snow bear alight with
                            his flaming sword but it continues to attack, forcing Thoros to get in its' way when it
                            attacks Sandor. Thoros is unable to break free of its jaws until Jorah stabs it with a
                            dragonglass dagger. Beric cauterizes Thoros' wounds with his flaming sword As a horde of
                            wights approach, Jon sends Gendry back to Eastwatch to bring news to Daenerys. The wights
                            pursue the group over a lake of ice. One of the ranging party is captured by the horde but
                            manages to drag many of the wights down with him when the ice collapses under their weight.
                            Gendry manages to outrun the wights after lending his hammer to Tormund. Meanwhile, Jon and
                            his ranging party manage to retreat to the middle of an icy lake to escape the wights.
                            Throughout the night, Jon and his comrades wait in the middle of the ice while encircled by
                            the army of the undead. Meanwhile, Gendry reaches Eastwatch's outer gates but collapses from
                            exhaustion. Davos Seaworth and several guards attend to him. When Davos asks what happened,
                            Gendry tells him to fetch the Maester and to send ravens to Daenerys. In the morning, Jon
                            and his company awake to find that Thoros has died from his wounds. Beric and Sandor pay
                            their last respects, with the former praying for the Lord of Light to guard them. At Jon's
                            insistence, they burn the body with Beric's flaming sword. The wights watch while their
                            captive Wight struggles under its hood and restraints. Jorah proposes killing the wight but
                            Jon counters that they need to keep it as evidence. Beric suggests that Jon kill the Night
                            King, who has just arrived on horseback; given that they've seen killing a Walker destroyed
                            the wights it controlled, killing the Night King might destroy them all. He then adds that
                            the Lord of Light has not resurrected Jon for no reason, but Sandor reminds him that they
                            have just lost their priest, and Beric is now down to his last life. Bored, Sandor hurls two
                            rocks at one of the undead minions, knocking its jaw off. The second however, skids across
                            the ice, and both the party and the undead quickly realize the ice, which has hardened
                            overnight, is strong enough to support their weight, and in ever-increasing numbers, the
                            horde attacks the group's position. Sandor holds them back with Gendry's hammer while Jon
                            and the other members of the ranging party join in, wielding weapons of fire, dragonglass or
                            Valyrian steel. Beric manages to set several of the wights alight with his flaming sword.
                            The ranging party hack and slash at the wights with their blades but are unable to stem the
                            tide. With the group overwhelmed, Jon orders them to fall back to the highest part of the
                            island. Tormund is overwhelmed by several wights and is nearly dragged under the ice, but is
                            saved by Sandor, who drags him back onto the island. The group continues fighting against
                            the wights. One of the Wildlings falls off a ledge and is ripped apart by the creatures, who
                            begin to scramble up the ledge towards the living. When all seems lost, Queen Daenerys
                            arrives with her dragons, who attack the wights with dragonfire. Hundreds of wights are
                            burned to ashes while others collapse under the ice, which is melted by dragonfire. Jon and
                            his party rush to Daenerys and her dragon Drogon, dragging their captive wight with them,
                            while Viserion and Rhaegal provide covering fire from above. Meanwhile, the Night King
                            obtains an icy javelin from one of his lieutenants and hurls it at Viserion, scoring a
                            direct hit. Viserion is struck in the neck and plunges helplessly into freefall, shrieking
                            in agony as blood and fire pour from the fatal wound. Drogon and Rhaegal cry out for their
                            brother, but are powerless to help him as Daenerys watches in horror and sorrow. Viserion
                            crashes onto the frozen lake, shattering the ice, and slowly sinks beneath it. As the Night
                            King readies another spear, Jon hollers at Daenerys and company to leave with her remaining
                            dragons before being dragged under the ice by two wights. Daenerys and the survivors of
                            Jon's ranging expedition flee with Drogon and Rhaegal before the Night King can kill them.
                            He hurls the second javelin, but Drogon narrowly dodges it. With the dragons gone, the Night
                            King and his army leave the scene. Later, Jon Snow climbs out of the ice and regains
                            Longclaw. Jon is quickly spotted and pursued by a large horde. Before the wights can finish
                            the King in the North, his long-lost uncle Benjen Stark appears on horseback with his
                            flaming flail. Benjen tells Jon to flee on his horse while he stays behind to buy time for
                            Jon to escape. While riding away on horseback, Jon watches his uncle being overwhelmed by
                            the undead. At Eastwatch Daenerys sends Drogon and Rhaegal to scour the surrounding
                            mountains for Jon. Jorah tells Daenerys that it is time to leave but she insists on waiting
                            a bit longer. Before she can leave, they hear a horn blowing signalling a rider approaching.
                            Looking down from the battlements, Dany sees a wounded Jon Snow approaching on horseback.
                            Aboard their ship, Davos and Gendry tend to Jon Snow, who has suffered severe hypothermia
                            and several minor injuries. Daenerys also notes the massive scars on his chest from his
                            previous fatal wounds.

                            In the Narrow Sea, Jon Snow wakes to find Daenerys watching over him in his chambers. Jon
                            apologizes for the disastrous ranging party and the fact it caused Viserion's death, but
                            Daenerys tells him not to apologize because she now knows that the Army of the Dead is real.
                            Overcome with emotion, she tells Jon that the dragons are the only children she will ever
                            have, and vows that she and Jon will together destroy the Night King. Jon thanks her for her
                            support, addressing her as "Dany", and Daenerys realizes that the last person to address her
                            by that name was her older brother Viserys Targaryen, who Daenerys remembers as not being a
                            good person. Jon apologizes and asks if "My Queen" would be more appropriate; realizing he
                            is agreeing to bend the knee, Daenerys asks Jon what the Northern lords loyal to him will
                            make of this. Jon assures her they will come to see her for the good person she is, as he
                            already has. Touched by his statement, Daenerys gently takes Jon's hand in her own for a
                            moment. They gaze into each other's eye for a moment – a long moment – but Daenerys suddenly
                            pulls away and tells him to get some rest and leaves him alone.

                            Following the events of the Wight Hunt, Sansa enters Arya's quarters and opens a leather
                            case containing several "faces", including the literally late Walder Frey's face. Arya
                            catches her sister pilfering through her personal effects. When Sansa tells Arya that her
                            men are loyal to her, Arya mockingly retorts that they are not here. Arya tells Sansa that
                            she obtained the faces from the Faceless Men of Braavos and admits she spent time training
                            to be a Faceless Man. She forces Sansa to play the lying game and begins by asking if she
                            thinks that Jon is the rightful King. Sansa demands that Arya tell her what the "faces" are.
                            Arya replies that they always wanted to pretend to be other people. Sansa wanted to be a
                            queen while Arya herself wanted to be a knight. In the end, neither of them got what they
                            wanted. Arya says that the faces allow her to become someone else and toys with the idea of
                            assuming Sansa's face and status. Arya approaches Sansa with her dagger and muses as the
                            possibility of becoming the Lady of Winterfell. However, Arya relents and leaves a disturbed
                            Sansa alone with the dagger.

                            Later at the Parley in King's Landing the various factions meet: Cersei Lannister, Jaime,
                            Qyburn, and Euron representing the Iron Throne, Jon, Davos and Brienne representing the
                            North, and Daenerys's court. After Daenerys arrives on Drogon they discuss the greater
                            threat at hand but she dismisses it as a ploy to trick her into lowering her defenses. To
                            prove their claims, Sandor returns with the crate containing the wight, which is worryingly
                            silent. Sandor gets the crate open, but there is still no movement. He finally gives the
                            crate a massive kick, which prompts the enraged wight to launch itself out and charge toward
                            the nearest target - Cersei, appropriately enough. Visibly horrified, the Lannister queen
                            and her allies recoil in horror as Sandor pulls the wight back on a chain, its claws inches
                            from Cersei's face, and manages to slice the creature in half when it turns to attack him.
                            The assembled look on in shock as the wight's upper half still moves around. Jon steps
                            forward and picks up the wight's discarded hand, using a torch provided by Davos to
                            demonstrate how fire can be used to stop them. He then uses a dragonglass dagger to the
                            heart to end the wight's upper half, bluntly stating that if they don't win the coming war,
                            such a fate awaits every person in Westeros. A horror-struck Jaime asks how many wights are
                            coming, and Daenerys tells him the army of the dead numbers at least 100,000. Seemingly
                            convinced, Cersei immediately offers terms: satisfied that Daenerys is concerned with the
                            Army of the Dead, Cersei will not withdraw her troops, but will guarantee that they will not
                            hinder the Targaryen or Northern forces in any way during the battle against the White
                            Walkers. She refuses to deal with Daenerys at all, however, and calls on Jon Snow, as King
                            in the North and Ned Stark's son, to keep the truce and to stay out of any future conflict
                            between Cersei and Daenerys. Jon, however, says that he cannot serve two queens - and
                            reveals to all assembled that he has already declared for Daenerys, infuriating all three
                            Lannisters present. Declaring that there will be no truce if it is just her and Daenerys,
                            Cersei storms out, content to let the Starks and Targaryens battle the undead alone and then
                            deal with whoever emerges victorious from that conflict. Desperate, Brienne grabs Jaime and
                            begs him to reconsider, as what they've seen goes beyond family, Houses, and thrones. Jaime
                            doesn't disagree, but walks away, not knowing what he can say to convince his sister.
                            Meanwhile, Daenerys and Tyrion (who never knew about Jon's change of heart in the first
                            place) rip into Jon over his ill-advised action, suggesting that learning to lie just a
                            little might be a good skill. Jon responds by arguing that while such an attitude may or may
                            not have contributed to getting his father killed, if no one is willing to speak the truth,
                            then everyone's word is worthless, and lies will not help them win the coming fight. Tyrion
                            reluctantly decides that he will go and try to talk some reason into Cersei alone. Daenerys
                            and Jon protest, fearing Cersei may have him killed out of spite, but Tyrion insists it's
                            the only way if they don't want everything they've done to be for nothing and bids them
                            wait. Back at the Dragonpit, Daenerys and Jon discuss the dragons and how her ancestors
                            caged them, and in turn become less impressive as the power of the dragons waned. Jon
                            questions Daenerys's assertion of infertility, particularly when she admits that she never
                            got an informed opinion about her condition from anyone except Mirri Maz Duur herself. Their
                            conversation is interrupted by the return of all three Lannisters. Cersei has agreed to work
                            with Daenerys, but not by keeping her troops back: the Lannister army will march north to
                            fight alongside the Starks and Targaryens. However, she plans to dishonor this truce as she
                            previously plotted with Euron to enlist the aid of the Golden Company to defend them once
                            the conflict in the North is over and the victor comes for the South. This however causes
                            Jaime to reach his breaking point and see his sister for who she really is: a power-mad
                            narcissist and leaves her to aid the North.

                            At Winterfell, Sansa Stark discusses the potential threat of her sister Arya Stark with
                            Petyr Baelish. Baelish tries to manipulate her as usual, encouraging her to think as he
                            does. He tells Sansa to ask herself what Arya's worst possible motivation is. Seemingly
                            overcome with horror at the thought that Arya would want to take her place and reign as Lady
                            of Winterfell, it seems that Sansa decides to do something about it, to Baelish's quiet
                            delight. After a long time reflecting on her course of action on the battlements, Sansa
                            orders Arya be brought to the great hall. In the Hall, Sansa and Bran are seated at the
                            great table, the hall lined with Stark and Arryn men and a few key lords such as Yohn Royce
                            and, of course, Baelish. Arya is brought in and asks Sansa if she "really wants to do this".
                            Sansa replies it's not about what she wants, it's about justice, and them proceeds to rattle
                            off a list of crimes perpetuated against House Stark... and asks Baelish how he intends to
                            answer the charges. At this, all eyes turn towards Baelish. Thrown, Littlefinger tries to
                            figure out what is going on. Sansa reveals his murder of Lysa Arryn and his use of Lysa to
                            murder Jon Arryn. She uses his own words against him and accuses him (quite correctly) of
                            orchestrating the conflict between the Starks and the Lannisters that has ultimately
                            engulfed the Seven Kingdoms for the better part of the last decade, including the betrayal
                            and death of her father Eddard Stark. Baelish tries to deny this, but Bran uses his
                            Greensight to recall the exact words Baelish said as he held the knife to Ned's throat.
                            Swiftly realizing that he has lost control of the situation and the trial is just a
                            drumhead, Baelish demands that Lord Royce take him, the Lord Protector of the Vale, to
                            safety; Bronze Yohn refuses him. In desperation, Baelish falls to his knees and pleads for
                            his life, insisting yet again how much he loved Cat and how much he now loves Sansa, but
                            Sansa, unswayed by his pleadings, sentences him to death and promises she will never forget
                            all the lessons he taught her. As Baelish tries to speak, Arya slits his throat with the
                            same Valyrian steel dagger that lay at the heart of his plots. Littlefinger slumps dead to
                            the floor, the Northern and Vale men and women watching dispassionately. On the battlements,
                            Sansa and Arya discuss Littlefinger's plots and how much they, as people, have changed. Arya
                            tells Sansa that she wouldn't have been able to survive what Sansa did, although her sister
                            disagrees, saying Arya is the strongest person she knows. The sisters muse on another of
                            their father's stories, about how lone wolves die in the winter, but wolf packs survive, and
                            realize the truth of his words as the Starks have at last been reunited.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@section('footer')
    <div class="col-lg-12 register-footer">
        <p>Designed by <span>Vladimir Diamandiev</span></p>
    </div>
@endsection