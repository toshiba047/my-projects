@extends('page.template')

@section('title', 'Can`t Log in ?')

@section('header')
<div class="top-layout">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12" >
                <div class="main-container">
                    <img src="/img/logo3.png">
                    <h1>House Stark FanSite</h1>
                    <div class="top-right links">
                        <a href="{{route('home')}}" class="home" id="home">Home</a>
                        <a href="{{url('/login')}}" id="login">Login</a>
                        <a href="/register">Register</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@section('body')
<div class="forgot_password">
  <h1>Enter Your Account Name or Email Adress</h1>
  <form method="post" id="" action="" enctype="">
    <div class="control-group">
      <label id="email-label" class="control-label" for="email"></label>
      <div class="controls">
      <input id="email" value="" name="email" title="Email Address" maxlength="" type="text" tabindex="1" class="input-block input-large " placeholder="Email Address" autocorrect="off" data-charmax-counter="NaN/NaN">
      </div>
    </div>
    <div class="control-group separated">
      <button type="submit" id="support-submit" class="btn btn-primary btn-large btn-response" data-loading-text="" tabindex="1">
      Continue
      </button>
      <a class="btn btn-large btn-response" href="#" tabindex="1">Cancel</a>
    </div>
      <input type="hidden" id="csrftoken" name="csrftoken" value="26c8d860-50f2-4fe6-a923-64ff8912a9bd">
  </form>
</div>
@section('footer')
<div class="col-lg-12 login-footer">
    <p>Designed by <span>Vladimir Diamandiev</span></p>
</div>
@endsection
