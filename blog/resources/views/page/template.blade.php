<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <title>House Stark | @yield('title')</title>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="{{url('css/app.css')}}">
        @stack('css-style')
    </head>
    <body>
        <header>
            @yield('header')
        </header>
            @yield('body') 
        <footer>
            @yield('footer')
        </footer>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="/js/app.js" type="text/javascript"></script>
        <script src="/js/owl.carousel.js" type="text/javascript"></script>



        {{-- 1000hz / bootstrap-validator --}}
        <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script>

        <!-- Toastr -->
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
        <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
        <script>
            $('#characterImage').on('change', function () {
                var ext = $(this).val().split('.').pop().toLowerCase();
                if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) === -1) {
                    $('#imagePreview').attr('src', '');
                    $('#characterImage').val('');
                    toastr.error('Invalid File!');
                }
                if (this.files[0].size > 2097152) {
                    $('#imagePreview').attr('src', '');
                    $('#characterImage').val('');
                    toastr.error('Image exceeds 2MB!');
                }
                else {
                    readImageURL(this);
                }
            });

            function readImageURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#imagePreview').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }
        </script>
        <script>
            @if (isset($errors) && count($errors) > 0)
            @foreach ($errors->all() as $error)
            toastr.error('{{ $error }}');
            @endforeach
            @endif

            @if (Session::has('successMessage'))
            toastr.success("{{Session::get('successMessage')}}");
            @endif

            @if (Session::has('errorMessage'))
            toastr.error("{{Session::get('errorMessage')}}");
            @endif
        </script>
    </body>
</html>
