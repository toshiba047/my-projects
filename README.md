# My Projects

## Project "NXT-PROJECT"

### General info
Site from .psd template with slider
	
### Technologies
The Project is created with:
* HTML
* CSS
* Bootstrap
* JavaScript
* jQuery
* The site has a slider and a mobile version

## Project "Movies-Catalog"

### General info
The site is a movie catalog.
	
### Technologies
The Project is created with:
* HTML
* CSS
* Bootstrap
* JavaScript
* jQuery
* The site has a slider and a mobile version

-------------------------------------------------------------------------------------------

## Project "Site-from-psd/Impressionist"

### General info
Basic site from .psd template.
	
### Technologies
The Project is created with:
* HTML
* CSS
* Bootstrap
* The site has a mobile version

-------------------------------------------------------------------------------------------

## Project "Site-from-psd-two/Appo_Project"

### General info
Site for phone apps from .psd template
	
### Technologies
The Project is created with:
* HTML
* CSS
* JavaScript
* jQuery
* Bootstrap
* The site has a mobile version

-------------------------------------------------------------------------------------------

## Project "Site-from-psd-tree/Unicorn"

### General info
Site from .psd template with two sliders
	
### Technologies
The Project is created with:
* HTML
* CSS
* JavaScript
* jQuery
* Bootstrap
* The site has a mobile version and two sliders

## Project "blog" - Laravel Project

### General info
Тhe site is a Laravel project and has been recently started. 
The project is under construction and is in the initial phase.
	
### Technologies
The Project is created with:
* HTML
* CSS
* JavaScript
* jQuery
* Bootstrap
* PHP
* MySQL	

### Setup
- The project requires installation via Composer.

	