//Slider

jssor_1_slider_init = function() {
  var jssor_1_options = {
    $AutoPlay: 1,
    $DragOrientation: 2,
    $PlayOrientation: 2,
    $ArrowNavigatorOptions: {
    $Class: $JssorArrowNavigator$
    },
    $BulletNavigatorOptions: {
    $Class: $JssorBulletNavigator$,
    $Orientation: 2
    }
  };

    var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

      /*#region responsive code begin*/

    var MAX_WIDTH = 1920;

      function ScaleSlider() {
        var containerElement = jssor_1_slider.$Elmt.parentNode;
        var containerWidth = containerElement.clientWidth;
          if (containerWidth) {
              var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);
                  jssor_1_slider.$ScaleWidth(expectedWidth);
          }
          else {
              window.setTimeout(ScaleSlider, 30);
                }
            }

        ScaleSlider();
        $Jssor$.$AddEvent(window, "load", ScaleSlider);
        $Jssor$.$AddEvent(window, "resize", ScaleSlider);
        $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };


//Mobile menu

  function myFunction(x) {
    x.classList.toggle("change");
  }

  $( document ).ready (function () {
  var $canvasBtn = $('#canvas-trigger');
  var $canvasMenu = $('#canvas-menu');


  $canvasBtn.on ('click',function(){
    if($canvasMenu.hasClass('open')){
      $canvasMenu.removeClass('open');
      $canvasBtn.removeClass('active');
      return;
    }

    $canvasMenu.addClass('open');
    $canvasBtn.addClass('active')
    return;
  });

});
